# IoT_BASICS 
>"The Internet of Things has the potential to change the world'
just as Intenet do so, or even more."

# Industrial Revolutions- Leading towards Iot...

>The Industrial Revolution was another one of the jumps forward,
in the story of civilization.

Inventions and Innovatios treaded towards a path of automation.
* Industrial revolution 1.0: Introduction to steam engine 
* Industrial revolution 2.0: Introduction to electrical giants
* Industrial revolution 3.0: Introduction to rapic automisation i.e PLCs
* Industrial revolution 4.0: Introduction to higher connectivity to internet, endeavouring IoTs.

![](https://www.tefen.com/uploads/1.png)

# Industry 3.0 
Around 1970 the Third Industrial Revolution involved the use of electronics and IT (Information Technology) to further automation in production. Manufacturing and automation advanced considerably thanks to Internet access, connectivity and renewable energy.

Industry 3.0 introduced more automated systems onto the assembly line to perform human tasks, i.e. using **Programmable Logic Controllers (PLC)**. Although automated systems were in place, they still relied on human input and intervention.

Basically , Industry 3.0 stores data in databases and represents in excel.

- **PLC**: Programmable logic controller , used to programe automatic systems by ladder diagram generally.



![](https://themanufacturer-cdn-1.s3.eu-west-2.amazonaws.com/wp-content/uploads/2019/12/14113222/Third.jpg)


## Industry 3.0 workflow:

![](https://www.adultimagroup.com/wp-content/uploads/2018/01/MES-software.png)

### Architecture:


1. **Input**:   

    * Sensors /Actuators  
        * Field transmitters for pressure,temperature ,flow,level,etc.  
        * Photoelectric Sensors  
        * Pressure switches,Push buttons,Vaccum switches,etc  
        * Encoders  
    
 

2. **Data Transfer/Fieldbus**:We take output of sensors/actuators and transfer to Our system using any one of this protocol:  
    * Communication Protocol  
        * ModBus  
        * ProfiBus/Industrial Ethernet  
        * Ethercat  
 

3. **Output Modules**:
    * Solenoid Valves
    * Motor shalves
    * Motor Drives/Motor starters

 Simulataneously we can plot this data in graph or store in excels also,using excels.

 >Industry 3.0 communication protocols:
![Industry 3.0 communicatio protocols](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR8bX0u3wV_NPsAKKHx57i4El08IhdeN6FO8g&usqp=CAU)

# Industy 4.0 (_INdustry 3.0---(internet)----industry4.0_)
> Industry 4.0 is Industry 3.0 devics connected to Internet,
what we call as IoT (internet of things).

Internet is an enormous source of data and information, when a device is connected to internet it gathers information, analyse it (if programed so)
and implement the action required.
This induces a sense of intelligence in devices and makes it smart, also called smart devices.

##what a device connected to internet can do:


![](https://i.pinimg.com/originals/3c/d0/62/3cd062365933a53128db31b8b86b01d6.png)

## Architechture 4.0:
>Data goes from controller to cloud via industrry 4.0 protocols.

![](https://i.pinimg.com/originals/e6/c6/9a/e6c69ad5912649a31831ff1a29ed8e9e.png)
### **Industry 4.0 protocols**
![](https://i1.wp.com/thestemblogsa.co.za/wp-content/uploads/2012/06/SSD_Fig_8_-_logos1.png?fit=500%2C342&ssl=1)

# Problems faced in upgradation of Industry 3.0 to Industry 4.0:
1. **COST**       : on switching from industry 3.0 to industry 4.0 devices ,exorbtanat budgest should be formulated,since both the devices are expensive.

2. **DOWNTIME**   : Changing hardware in a factory means a longer downtime for factory hence compensating loss.

3. **reliability**: IoT are not yet proven to be reliable , more reseach is needed , so intalling it on such a large scale could cause harm to factory and business.


#### Solution to above problems:  
* Get data from Industry 3.0 devices/meters/sensors without changes to the original device.    
* And then send the data to the Cloud using Industry 4.0 devices.  

# How to make your own Industrial IOt Product:  
1. Identify most popular Industry 3.0 devices.
2. Study the protocols that these devices communicate.  
3. Get data from these devices.  
4. Send the data to cloud for industry 4.0  








